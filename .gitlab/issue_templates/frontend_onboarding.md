Welcome to your Frontend onboarding issue! We are so excited you're here! 

#### Frontend Engineers

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Familiarize yourself with the [frontend development guidelines](https://docs.gitlab.com/ce/development/fe_guide/).
1. [ ] New team member: Familiarize yourself with the [Vue documentation](https://vuejs.org/v2/guide/).
1. [ ] New team member: Familiarize yourself with [GitLab UI](https://gitlab-org.gitlab.io/gitlab-ui).
1. [ ] New team member: Familiarize yourself with [GitLab's Design System](http://design.gitlab.com).
1. [ ] Your specific Frontend Team Buddy is: XXX
1. [ ] New team member: Join the [#frontend](https://gitlab.slack.com/messages/frontend/) channel on Slack.
1. [ ] New team member: Join the ['#eng-week-in-review' channel](https://app.slack.com/client/T02592416/CJWA4E9UG)
1. [ ] New Team Member: Start off with these issues and check in with your buddy every second day on your progress, what you have done and your final MR.
1. [ ] New Team Member: Put on the agenda in your first week a small FE intro. What have you done before specifically? Favorite Frameworks? Your Code Editor? etc.
1. [ ] New Team Member: Check in with your respective manager on which areas you will start working so that you can take a look at the functionality and read the documentation about it.

</details>

<details>
<summary>Manager</summary>

1. [ ] Manager: Grant new team member edit access to the frontend calendar
1. [ ] Manager: Add new team member to [@gl-frontend](https://gitlab.com/groups/gl-frontend/-/group_members) group

</details>

<details>
<summary>FE Buddy</summary>

1. [ ] FE Buddy: Schedule an introduction [coffee break call](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) in advance for the first 2 days.
1. [ ] FE Buddy: Do an intro session about the GDK and specifically about our FE Tools (for example best way to run Tests, etc.).
1. [ ] FE Buddy: Show one of your current deliverables and explain based on that: What you have actually done, How did you find the actual relevant files, reproducing steps, How our process works, How to submit an MR, When to involve reviewers etc.
1. [ ] FE Buddy: Search for 3 easy issues to start with (if none found, create one based on existing code) which is a simple refactoring, removal of ESLint exceptions, replace Modals or Icons.

</details>

/confidential
